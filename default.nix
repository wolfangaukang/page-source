{ pkgs, ... }:

let
  inherit (pkgs) stdenv lib hugo;

in
stdenv.mkDerivation {
  name = "page";
  src = lib.cleanSource ./.;

  buildInputs = [ hugo ];
}
