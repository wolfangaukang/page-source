#!/usr/bin/env just --justfile
CONTENT_DIR:='public/'
DESTINATION_DIR:='pages'

# Show all recipes
default:
	@just --list

# Git Actions

# Inits submodules (useful for themes)
git-init-submodules:
	@git submodule init

# Updates submodules
git-update-submodules:
	@git submodule update --remote --recursive

# Hugo Actions

# Serves pages with drafts through Hugo
hugo-serve-d:
	@hugo server -D

# Serves final page through Hugo
hugo-serve:
	@hugo server

# Builds the public/ content
hugo-build:
	@hugo --minify

# Deploy to pages
deploy: hugo-build
	#!/usr/bin/env bash
	set -euo pipefail
	git config --global user.email "mail@ci.codeberg.org"
	git config --global user.name "Codeberg CI"

	git clone https://"$PAGES_ACCESS_TOKEN"@codeberg.org/wolfangaukang/pages.git {{DESTINATION_DIR}} 
	rsync -r --delete --exclude-from=".rsyncignore" {{CONTENT_DIR}} {{DESTINATION_DIR}}

	cd {{DESTINATION_DIR}} 
	git add -A
	git commit -m "Auto-commit from ${CI_REPO} (${CI_COMMIT_SHA})"
	git push origin main
