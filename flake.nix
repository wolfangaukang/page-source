{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    let
      overlays = [
        (final: prev: { page = final.callPackage ./default.nix { }; })
      ];

    in
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system overlays; };

        serve-function = { extra_opts ? "" }:
        let
          function = pkgs.writeShellScriptBin "serve" ''
            export PORT="''${HUGO_PORT:-8080}"
            echo "Serving Hugo on localhost:$PORT with the options: ${extra_opts}"
            ${pkgs.hugo}/bin/hugo server --port "$PORT" ${extra_opts} 
          '';
        in function;

        # Complex scripts
        serve-d = serve-function { extra_opts = "-D"; }; 
        serve = serve-function { };

      in rec {
        apps = {
          serve = {
            type = "app";
            program = "${serve}/bin/serve";
          };
          serve-draft = {
            type = "app";
            program = "${serve-d}/bin/serve";
          };

          default = apps.serve;
        };

        packages.default = pkgs.page;

        devShells.default = pkgs.mkShell {
          inputsFrom = [ packages.default ];
          buildInputs = with pkgs; [ just rsync ];
        };
      }
    );
}
